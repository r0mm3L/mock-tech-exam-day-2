
function countLetter(letter, sentence) {
    let result = 0;
    let letterCount = letter.length
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
    console.log(letter)
    if(letterCount == 1){
        console.log((sentence.split(letter).length - 1));
    } else {
        return undefined
    }


    
}


    
const invalidLetter = 'abc';
const sentence1 = 'The quick brown fox jumps over the lazy dog';
const validLetter = 'o';
// const invalidLetter
// const sentence1 
// const validLetter
let letter = validLetter
let sentence = sentence1

countLetter(letter, sentence)


function isIsogram() {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    
}

let studentPeople = 15
let seniorPeople = 66
let middlePeople = 30
let price = 100
function purchase() {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    if(studentPeople <= 13){
        return undefined
    } if( studentPeople >= 13 && studentPeople <= 21) {
        let discountedPrice1 = price * 0.2
        let roundedOff1 = Math.round(discountedPrice1)
        return roundedOff1
    } if(seniorPeople >= 65){
        let discountedPrice2 = price * 0.2
        let roundedOff2 = Math.round(discountedPrice2)
        return roundedOff2
    }  if( middlePeople >= 22 && middlePeople <=64) {
        let price3 = price
        let roundedOff3 = Math.round(price3)
        return roundedOff3
    }
}

purchase()

function findHotCategories() {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
var object = 
    [
        { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
        { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
        { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
        { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
        { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    ]

    // var toFind = "0";
    // for (var i = 0, len = object.main.length; i < len; i++) {
    //   if (object.main[i].id === toFind) {
    //     break;
    //   }
    // }
    // console.log(object.main[i].category);
    
    function isStocks(personals) {
        return personals.stocks === '0';
      }
      console.log(object.find(isStocks));

}

findHotCategories()

function findFlyingVoters() {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};